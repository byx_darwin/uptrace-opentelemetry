module gitee.com/byx_darwin/uptrace-opentelemetry/log

go 1.22.0

toolchain go1.23.6

require (
	github.com/cloudwego/hertz v0.9.6
	github.com/cloudwego/kitex v0.12.3
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/pkg/errors v0.9.1
	go.opentelemetry.io/otel v1.35.0
	go.opentelemetry.io/otel/trace v1.35.0
	go.uber.org/zap v1.27.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)

require (
	github.com/lestrrat-go/strftime v1.1.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
)
