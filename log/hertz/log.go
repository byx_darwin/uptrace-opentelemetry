package hertz

import (
	"context"
	"fmt"
	"github.com/cloudwego/hertz/pkg/common/hlog"
	"github.com/pkg/errors"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io"
	"runtime"
)

var _ hlog.FullLogger = (*Logger)(nil)

const (
	traceIDKey    = "trace_id"
	spanIDKey     = "span_id"
	traceFlagsKey = "trace_flags"
	logEventKey   = "log"
)

var (
	logSeverityKey = attribute.Key("log.severity")
	logMessageKey  = attribute.Key("log.message")
)

type Logger struct {
	*zap.SugaredLogger
	config *config
}

func NewLogger(opts ...Option) *Logger {
	config := defaultConfig()
	// apply options
	for _, opt := range opts {
		opt.apply(config)
	}

	cores := make([]zapcore.Core, 0, len(config.coreConfigs))
	for _, coreConfig := range config.coreConfigs {
		cores = append(cores, zapcore.NewCore(coreConfig.Enc, coreConfig.Ws, coreConfig.Lvl))
	}
	logger := zap.New(
		zapcore.NewTee(cores[:]...),
		config.zapOpts...,
	)

	return &Logger{
		SugaredLogger: logger.Sugar(),
		config:        config,
	}
}

// GetExtraKeys get extraKeys from logger config
func (l *Logger) GetExtraKeys() []ExtraKey {
	return l.config.extraKeys
}

// PutExtraKeys add extraKeys after init
func (l *Logger) PutExtraKeys(keys ...ExtraKey) {
	for _, k := range keys {
		if !inArray(k, l.config.extraKeys) {
			l.config.extraKeys = append(l.config.extraKeys, k)
		}
	}
}

func (l *Logger) Log(level hlog.Level, kvs ...interface{}) {
	logger := l.With()
	switch level {
	case hlog.LevelTrace, hlog.LevelDebug:
		logger.Debug(kvs...)
	case hlog.LevelInfo:
		logger.Info(kvs...)
	case hlog.LevelNotice, hlog.LevelWarn:
		logger.Warn(kvs...)
	case hlog.LevelError:
		logger.Error(kvs...)
	case hlog.LevelFatal:
		logger.Fatal(kvs...)
	default:
		logger.Warn(kvs...)
	}
}

func (l *Logger) Logf(level hlog.Level, format string, kvs ...interface{}) {
	logger := l.With()
	switch level {
	case hlog.LevelTrace, hlog.LevelDebug:
		logger.Debugf(format, kvs...)
	case hlog.LevelInfo:
		logger.Infof(format, kvs...)
	case hlog.LevelNotice, hlog.LevelWarn:
		logger.Warnf(format, kvs...)
	case hlog.LevelError:
		logger.Errorf(format, kvs...)
	case hlog.LevelFatal:
		logger.Fatalf(format, kvs...)
	default:
		logger.Warnf(format, kvs...)
	}
}

func (l *Logger) CtxLogf(level hlog.Level, ctx context.Context, format string, kvs ...interface{}) {
	var zlevel zapcore.Level
	var sl *zap.SugaredLogger

	span := trace.SpanFromContext(ctx)
	var traceKVs []interface{}
	if span.SpanContext().TraceID().IsValid() {
		traceKVs = append(traceKVs, traceIDKey, span.SpanContext().TraceID())
	}
	if span.SpanContext().SpanID().IsValid() {
		traceKVs = append(traceKVs, spanIDKey, span.SpanContext().SpanID())
	}
	if span.SpanContext().TraceFlags().IsSampled() {
		traceKVs = append(traceKVs, traceFlagsKey, span.SpanContext().TraceFlags())
	}
	if len(traceKVs) > 0 {
		sl = l.With(traceKVs...)
	} else {
		sl = l.With()
	}
	if len(l.config.extraKeys) > 0 {
		for _, k := range l.config.extraKeys {
			if l.config.extraKeyAsStr {
				sl = sl.With(string(k), ctx.Value(string(k)))
			} else {
				sl = sl.With(string(k), ctx.Value(k))
			}
		}
	}
	switch level {
	case hlog.LevelDebug, hlog.LevelTrace:
		zlevel = zap.DebugLevel
		sl.Debugf(format, kvs...)
	case hlog.LevelInfo:
		zlevel = zap.InfoLevel
		sl.Infof(format, kvs...)
	case hlog.LevelNotice, hlog.LevelWarn:
		zlevel = zap.WarnLevel
		sl.Warnf(format, kvs...)
	case hlog.LevelError:
		zlevel = zap.ErrorLevel
		sl.Errorf(format, kvs...)
	case hlog.LevelFatal:
		zlevel = zap.FatalLevel
		sl.Fatalf(format, kvs...)
	default:
		zlevel = zap.WarnLevel
		sl.Warnf(format, kvs...)
	}
	if !span.IsRecording() || zlevel < l.config.traceConfig.minSpanLevel {
		return
	}
	msg := getMessage(format, kvs)
	//attrs := []attribute.KeyValue{
	//	logMessageKey.String(msg),
	//
	//	//logSeverityTextKey.String(OtelSeverityText(zlevel)),
	//}
	attrs := make([]attribute.KeyValue, 0)
	attrs = append(attrs, logMessageKey.String(msg))
	attrs = append(attrs, logSeverityKey.String(OtelSeverityText(zlevel)))

	if fn, file, line, ok := runtimeCaller(4); ok {
		if fn != "" {
			attrs = append(attrs, semconv.CodeFunctionKey.String(fn))
		}
		if file != "" {
			attrs = append(attrs, semconv.CodeFilepathKey.String(file))
			attrs = append(attrs, semconv.CodeLineNumberKey.Int(line))
		}
	}

	span.AddEvent(logEventKey, trace.WithAttributes(attrs...))

	// set span status
	if zlevel >= l.config.traceConfig.errorSpanLevel {
		span.SetStatus(codes.Error, msg)
		span.RecordError(errors.New(msg), trace.WithStackTrace(l.config.traceConfig.recordStackTraceInSpan))
	}

}

func (l *Logger) Trace(v ...interface{}) {
	l.Log(hlog.LevelTrace, v...)
}

func (l *Logger) Debug(v ...interface{}) {
	l.Log(hlog.LevelDebug, v...)
}

func (l *Logger) Info(v ...interface{}) {
	l.Log(hlog.LevelInfo, v...)
}

func (l *Logger) Notice(v ...interface{}) {
	l.Log(hlog.LevelNotice, v...)
}

func (l *Logger) Warn(v ...interface{}) {
	l.Log(hlog.LevelWarn, v...)
}

func (l *Logger) Error(v ...interface{}) {
	l.Log(hlog.LevelError, v...)
}

func (l *Logger) Fatal(v ...interface{}) {
	l.Log(hlog.LevelFatal, v...)
}

func (l *Logger) Tracef(format string, v ...interface{}) {
	l.Logf(hlog.LevelTrace, format, v...)
}

func (l *Logger) Debugf(format string, v ...interface{}) {
	l.Logf(hlog.LevelDebug, format, v...)
}

func (l *Logger) Infof(format string, v ...interface{}) {
	l.Logf(hlog.LevelInfo, format, v...)
}

func (l *Logger) Noticef(format string, v ...interface{}) {
	l.Logf(hlog.LevelWarn, format, v...)
}

func (l *Logger) Warnf(format string, v ...interface{}) {
	l.Logf(hlog.LevelWarn, format, v...)
}

func (l *Logger) Errorf(format string, v ...interface{}) {
	l.Logf(hlog.LevelError, format, v...)
}

func (l *Logger) Fatalf(format string, v ...interface{}) {
	l.Logf(hlog.LevelFatal, format, v...)
}

func (l *Logger) CtxTracef(ctx context.Context, format string, v ...interface{}) {
	l.CtxLogf(hlog.LevelDebug, ctx, format, v...)
}

func (l *Logger) CtxDebugf(ctx context.Context, format string, v ...interface{}) {
	l.CtxLogf(hlog.LevelDebug, ctx, format, v...)
}

func (l *Logger) CtxInfof(ctx context.Context, format string, v ...interface{}) {
	l.CtxLogf(hlog.LevelInfo, ctx, format, v...)
}

func (l *Logger) CtxNoticef(ctx context.Context, format string, v ...interface{}) {
	l.CtxLogf(hlog.LevelWarn, ctx, format, v...)
}

func (l *Logger) CtxWarnf(ctx context.Context, format string, v ...interface{}) {
	l.CtxLogf(hlog.LevelWarn, ctx, format, v...)
}

func (l *Logger) CtxErrorf(ctx context.Context, format string, v ...interface{}) {
	l.CtxLogf(hlog.LevelError, ctx, format, v...)
}

func (l *Logger) CtxFatalf(ctx context.Context, format string, v ...interface{}) {
	l.CtxLogf(hlog.LevelFatal, ctx, format, v...)
}

func (l *Logger) SetLevel(level hlog.Level) {
	var lvl zapcore.Level
	switch level {
	case hlog.LevelTrace, hlog.LevelDebug:
		lvl = zap.DebugLevel
	case hlog.LevelInfo:
		lvl = zap.InfoLevel
	case hlog.LevelWarn, hlog.LevelNotice:
		lvl = zap.WarnLevel
	case hlog.LevelError:
		lvl = zap.ErrorLevel
	case hlog.LevelFatal:
		lvl = zap.FatalLevel
	default:
		lvl = zap.WarnLevel
	}
	l.config.coreConfigs[0].Lvl = lvl
	cores := make([]zapcore.Core, 0, len(l.config.coreConfigs))
	for _, coreConfig := range l.config.coreConfigs {
		cores = append(cores, zapcore.NewCore(coreConfig.Enc, coreConfig.Ws, coreConfig.Lvl))
	}

	logger := zap.New(
		zapcore.NewTee(cores[:]...),
		l.config.zapOpts...)

	l.SugaredLogger = logger.Sugar()
}

func (l *Logger) SetOutput(writer io.Writer) {
	l.config.coreConfigs[0].Ws = zapcore.AddSync(writer)

	cores := make([]zapcore.Core, 0, len(l.config.coreConfigs))
	for _, coreConfig := range l.config.coreConfigs {
		cores = append(cores, zapcore.NewCore(coreConfig.Enc, coreConfig.Ws, coreConfig.Lvl))
	}

	logger := zap.New(
		zapcore.NewTee(cores[:]...),
		l.config.zapOpts...)

	l.SugaredLogger = logger.Sugar()
}
func (l *Logger) CtxKVLog(ctx context.Context, level hlog.Level, format string, kvs ...interface{}) {
	if len(kvs) == 0 || len(kvs)%2 != 0 {
		l.Warn(fmt.Sprint("Keyvalues must appear in pairs:", kvs))
		return
	}

	span := trace.SpanFromContext(ctx)
	if span.SpanContext().TraceID().IsValid() {
		kvs = append(kvs, traceIDKey, span.SpanContext().TraceID())
	}
	if span.SpanContext().SpanID().IsValid() {
		kvs = append(kvs, spanIDKey, span.SpanContext().SpanID())
	}
	if span.SpanContext().TraceFlags().IsSampled() {
		kvs = append(kvs, traceFlagsKey, span.SpanContext().TraceFlags())
	}
	var zlevel zapcore.Level
	zl := l.With()
	switch level {
	case hlog.LevelDebug, hlog.LevelTrace:
		zlevel = zap.DebugLevel
		zl.Debugw(format, kvs...)
	case hlog.LevelInfo:
		zlevel = zap.InfoLevel
		zl.Infow(format, kvs...)
	case hlog.LevelNotice, hlog.LevelWarn:
		zlevel = zap.WarnLevel
		zl.Warnw(format, kvs...)
	case hlog.LevelError:
		zlevel = zap.ErrorLevel
		zl.Errorw(format, kvs...)
	case hlog.LevelFatal:
		zlevel = zap.FatalLevel
		zl.Fatalw(format, kvs...)
	default:
		zlevel = zap.WarnLevel
		zl.Warnw(format, kvs...)
	}

	if !span.IsRecording() || zlevel < l.config.traceConfig.minSpanLevel {
		return
	}

	msg := getMessage(format, kvs)
	attrs := make([]attribute.KeyValue, 0)
	attrs = append(attrs, logMessageKey.String(msg))
	attrs = append(attrs, logSeverityKey.String(OtelSeverityText(zlevel)))

	if fn, file, line, ok := runtimeCaller(4); ok {
		if fn != "" {
			attrs = append(attrs, semconv.CodeFunctionKey.String(fn))
		}
		if file != "" {
			attrs = append(attrs, semconv.CodeFilepathKey.String(file))
			attrs = append(attrs, semconv.CodeLineNumberKey.Int(line))
		}
	}
	fmt.Println(attrs)
	// notice: AddEvent,SetStatus,RecordError all have check span.IsRecording
	span.AddEvent(logEventKey, trace.WithAttributes(attrs...))
	// set span status
	if zlevel >= l.config.traceConfig.errorSpanLevel {
		span.SetStatus(codes.Error, msg)
		span.RecordError(errors.New(msg), trace.WithStackTrace(l.config.traceConfig.recordStackTraceInSpan))
	}

}

func runtimeCaller(skip int) (fn, file string, line int, ok bool) {
	rpc := make([]uintptr, 1)
	n := runtime.Callers(skip+1, rpc[:])
	if n < 1 {
		return
	}
	frame, _ := runtime.CallersFrames(rpc).Next()
	return frame.Function, frame.File, frame.Line, frame.PC != 0
}
