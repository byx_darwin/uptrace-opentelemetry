package log

import (
	Kitexzap "gitee.com/byx_darwin/uptrace-opentelemetry/log/kitex"
	"github.com/cloudwego/kitex/pkg/klog"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"time"
)

func NewKitexLog(logConfig Config) {
	opts := make([]Option, 0, 8)
	opts = append(opts, WithPath(logConfig.Path))
	opts = append(opts, WithMaxSize(logConfig.MaxSize))
	opts = append(opts, WithMaxBackups(logConfig.MaxBackups))
	opts = append(opts, WithMaxAge(logConfig.MaxAge))
	opts = append(opts, WithCompress(logConfig.Compress))
	opts = append(opts, WithOutputMode(logConfig.OutputMode))
	opts = append(opts, WithRotationDuration(time.Duration(logConfig.RotationDuration)))
	opts = append(opts, WithSuffix(logConfig.Suffix))
	config := DefaultOptionConfig()
	for _, opt := range opts {
		opt.apply(config)
	}
	dynamicLevel := zap.NewAtomicLevel()
	dynamicLevel.SetLevel(zap.DebugLevel)
	coreConfigs := make([]Kitexzap.CoreConfig, 0)
	switch config.outputMode {
	case 1:
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewConsoleEncoder(pkgEncoderConfig()),
			Ws:  zapcore.AddSync(os.Stdout),
			Lvl: dynamicLevel,
		})
	case 2:
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/all/app", config),
			Lvl: zap.NewAtomicLevelAt(zapcore.DebugLevel),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/debug/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev == zap.DebugLevel
			}),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/info/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev == zap.InfoLevel
			}),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/warn/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev == zap.WarnLevel
			}),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/error/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev == zap.ErrorLevel
			}),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/panic/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev >= zap.DPanicLevel
			}),
		})
	case 3:
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewConsoleEncoder(pkgEncoderConfig()),
			Ws:  zapcore.AddSync(os.Stdout),
			Lvl: dynamicLevel,
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/all/app", config),
			Lvl: zap.NewAtomicLevelAt(zapcore.DebugLevel),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/debug/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev == zap.DebugLevel
			}),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/info/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev == zap.InfoLevel
			}),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/warn/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev == zap.WarnLevel
			}),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/error/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev == zap.ErrorLevel
			}),
		})
		coreConfigs = append(coreConfigs, Kitexzap.CoreConfig{
			Enc: zapcore.NewJSONEncoder(pkgEncoderConfig()),
			Ws:  NewWriteSyncer("/panic/app", config),
			Lvl: zap.LevelEnablerFunc(func(lev zapcore.Level) bool {
				return lev >= zap.DPanicLevel
			}),
		})
	}
	logger := Kitexzap.NewLogger(Kitexzap.WithCores(coreConfigs...),
		Kitexzap.WithTraceMinSpanLevel(Level(logConfig.MinSpanLevel).ZapLevel()),
		Kitexzap.WithTraceErrorSpanLevel(Level(logConfig.ErrorSpanLevel).ZapLevel()),
		Kitexzap.WithRecordStackTraceInSpan(logConfig.RecordStack),
		Kitexzap.WithZapOptions(zap.AddCaller(), zap.AddCallerSkip(3)),
	)

	defer func(logger *Kitexzap.Logger) {
		err := logger.Sync()
		if err != nil {
			logger.Error(err)
		}
	}(logger)
	klog.SetLogger(logger)
	klog.SetLevel(Level(logConfig.MinSpanLevel).KLogLevel())
}
