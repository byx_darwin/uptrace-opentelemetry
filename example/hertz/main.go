package main

import (
	"context"
	"gitee.com/byx_darwin/uptrace-opentelemetry/cloudwego/hertz/otelhertz"
	"gitee.com/byx_darwin/uptrace-opentelemetry/otelplay"

	"gitee.com/byx_darwin/uptrace-opentelemetry/log"
	"github.com/cloudwego/hertz/pkg/app"
	"github.com/cloudwego/hertz/pkg/app/server"
	"github.com/cloudwego/hertz/pkg/common/hlog"
	"github.com/cloudwego/hertz/pkg/common/utils"
	hertztracing "github.com/hertz-contrib/obs-opentelemetry/tracing"
	"go.opentelemetry.io/otel/trace"
	"html/template"
	"net/http"
)

const (
	indexTmpl   = "index"
	profileTmpl = "profile"
)

func main() {
	ctx := context.Background()
	shutdown := otelplay.ConfigureOpentelemetry(ctx, &otelplay.Config{
		ServiceDSN:     "http://mT6XJEgsNqTs6N0GKmqOIStqlZKMGXpK@47.113.231.80:14318?grpc=14317",
		ServiceName:    "hertz-example",
		ServiceVersion: "v1.0.0",
		Environment:    "dev",
	})
	defer shutdown()
	log.NewHertzLog(log.Config{
		Path:             "./log",
		MaxSize:          10,
		MaxBackups:       10,
		MaxAge:           30,
		Compress:         false,
		OutputMode:       2,
		Suffix:           ".log",
		RotationDuration: 0,
		MinSpanLevel:     "debug",
		ErrorSpanLevel:   "error",
		RecordStack:      false,
	})
	tracer, cfg := hertztracing.NewServerTracer()
	h := server.Default(tracer)
	h.Use(hertztracing.ServerMiddleware(cfg))
	h.SetHTMLTemplate(parseTemplates())
	h.GET("/", indexHandler)
	h.GET("/hello/:username", helloHandler)
	h.Spin()
}
func parseTemplates() *template.Template {
	indexTemplate := `
		<html>
		<p>Here are some routes for you:</p>
		<ul>
			<li><a href="/hello/world">Hello world</a></li>
			<li><a href="/hello/foo-bar">Hello foo-bar</a></li>
		</ul>
		<p><a href="{{ .traceURL }}" target="_blank">{{ .traceURL }}</a></p>
		</html>
	`
	t := template.Must(template.New(indexTmpl).Parse(indexTemplate))

	profileTemplate := `
		<html>
		<h3>Hello {{ .username }}</h3>
		<p><a href="{{ .traceURL }}" target="_blank">{{ .traceURL }}</a></p>
		</html>
	`
	return template.Must(t.New(profileTmpl).Parse(profileTemplate))
}

func indexHandler(c context.Context, ctx *app.RequestContext) {
	otelhertz.HTML(c, ctx, http.StatusOK, indexTmpl,
		utils.H{
			"traceURL": otelplay.TraceURL(trace.SpanFromContext(c)),
		})
}

func helloHandler(c context.Context, ctx *app.RequestContext) {
	hlog.CtxErrorf(c, "foo:%v", "bar")
	otelhertz.HTML(c, ctx, http.StatusOK, profileTmpl, utils.H{
		"username": ctx.Param("username"),
		"traceURL": otelplay.TraceURL(trace.SpanFromContext(c)),
	})
}
