module gitee.com/byx_darwin/uptrace-opentelemetry/example/hertz

go 1.21.3

require (
	gitee.com/byx_darwin/uptrace-opentelemetry/cloudwego/hertz/otelhertz v0.0.0-20240122022130-3ba21dc80d5f
	gitee.com/byx_darwin/uptrace-opentelemetry/log v0.0.1
	gitee.com/byx_darwin/uptrace-opentelemetry/otelplay v0.0.0-20240122022130-3ba21dc80d5f
	github.com/cloudwego/hertz v0.9.0
	github.com/hertz-contrib/obs-opentelemetry/tracing v0.3.1
	go.opentelemetry.io/otel/trace v1.26.0
)

require (
	github.com/andeya/goutil v1.0.0 // indirect
	github.com/bytedance/go-tagexpr/v2 v2.9.6 // indirect
	github.com/bytedance/gopkg v0.0.0-20231219111115-a5eedbe96960 // indirect
	github.com/bytedance/sonic v1.11.2 // indirect
	github.com/cenkalti/backoff/v4 v4.3.0 // indirect
	github.com/chenzhuoyu/base64x v0.0.0-20230717121745-296ad89f973d // indirect
	github.com/chenzhuoyu/iasm v0.9.1 // indirect
	github.com/cloudwego/kitex v0.9.1 // indirect
	github.com/cloudwego/netpoll v0.6.0 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/go-logr/logr v1.4.1 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.19.1 // indirect
	github.com/henrylee2cn/ameda v1.5.1 // indirect
	github.com/klauspost/cpuid/v2 v2.2.4 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible // indirect
	github.com/lestrrat-go/strftime v1.0.6 // indirect
	github.com/nyaruka/phonenumbers v1.1.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/tidwall/gjson v1.14.4 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/twitchyliquid64/golang-asm v0.15.1 // indirect
	github.com/uptrace/uptrace-go v1.26.2 // indirect
	go.opentelemetry.io/contrib/instrumentation/runtime v0.51.0 // indirect
	go.opentelemetry.io/otel v1.26.0 // indirect
	go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp v1.26.0 // indirect
	go.opentelemetry.io/otel/exporters/otlp/otlptrace v1.26.0 // indirect
	go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp v1.26.0 // indirect
	go.opentelemetry.io/otel/exporters/stdout/stdouttrace v1.26.0 // indirect
	go.opentelemetry.io/otel/metric v1.26.0 // indirect
	go.opentelemetry.io/otel/sdk v1.26.0 // indirect
	go.opentelemetry.io/otel/sdk/metric v1.26.0 // indirect
	go.opentelemetry.io/proto/otlp v1.2.0 // indirect
	go.uber.org/goleak v1.3.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.26.0 // indirect
	golang.org/x/arch v0.2.0 // indirect
	golang.org/x/net v0.25.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20240506185236-b8a5c65736ae // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240506185236-b8a5c65736ae // indirect
	google.golang.org/grpc v1.63.2 // indirect
	google.golang.org/protobuf v1.34.1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
)

replace (
	gitee.com/byx_darwin/uptrace-opentelemetry/cloudwego/hertz/otelhertz v0.0.0-20240122022130-3ba21dc80d5f => ../../cloudwego/hertz/otelhertz
	gitee.com/byx_darwin/uptrace-opentelemetry/log v0.0.1 => ../../log
	gitee.com/byx_darwin/uptrace-opentelemetry/otelplay v0.0.0-20240122022130-3ba21dc80d5f => ../../otelplay
)
