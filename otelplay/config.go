package otelplay

type Config struct {
	ServiceDSN     string
	ServiceName    string
	ServiceVersion string
	Environment    string
}
