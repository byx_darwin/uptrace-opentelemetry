package otelplay

import (
	"context"
	"fmt"
	"github.com/uptrace/uptrace-go/uptrace"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

func PrintTraceID(ctx context.Context) {
	fmt.Println("trace:", TraceURL(trace.SpanFromContext(ctx)))
}

func TraceURL(span trace.Span) string {
	return uptrace.TraceURL(span)
}

// ConfigureOpentelemetry configures Opentelemetry to export spans to Uptrace, Jaeger,
// or console depending on environment variables.
//
// You can use it to run examples, but don't use it in your applications. Instead, use
// uptrace-go or opentelemetry-go directly. See https://uptrace.dev/get/opentelemetry-go.html
func ConfigureOpentelemetry(ctx context.Context, conf *Config) func() {
	uptrace.ConfigureOpentelemetry(uptrace.WithDSN(conf.ServiceDSN),
		uptrace.WithServiceName(conf.ServiceName),
		uptrace.WithServiceVersion(conf.ServiceVersion), uptrace.WithResourceAttributes(
			attribute.String("deployment.environment", conf.Environment),
		))
	return func() {
		_ = uptrace.Shutdown(ctx)
	}

}
